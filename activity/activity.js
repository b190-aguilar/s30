// Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
                {$match: {onSale : true}},
                {$group: {_id: "", fruitsOnSale: {$count: {}}}},
                {$project: { _id: 0}}
	]);
                



// Use the count operator to count the total number of fruits with stock more than 20.


db.fruits.aggregate([
                {$match: {stock: {$gt: 20}}},
                {$group: {_id:  "" , enoughStock: {$count: {}}}},
                {$project: { _id: 0}}
	]);



// Use the average operator to get the average price of fruits onSale per supplier.



db.fruits.aggregate([
	{$match: {onSale : true}},
	{$group: {_id:  "$supplier_id" , averagePrice: {$avg: "$price" }}},
]);




// Use the max operator to get the highest price of a fruit per supplier. 

db.fruits.aggregate([
	{$group: {_id:  "$supplier_id" , max_price: {$max: "$price" }}},
]);

	


//  Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
	{$group: {_id:  "$supplier_id" , min_price: {$min: "$price" }}},
]);
