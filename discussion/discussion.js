

// SECTION - MongoDB Aggregation
/*
 	- used to generate manipulated data and perform operations to create filtered results that help in analyzing data.

 	- compared to CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application.
*/




// SECTION - using AGGREGATE Method
/*
	$match 
	- used to pass the documents that meet the specified condition(s) to the next pipeline/aggregation
	
	SYNTAX:
		{ $match: { field: value } }
*/


db.fruits.aggregate([
		{$match: {onSale : true} }
]);


/*
	$group
	- used to group elements together and field-value pairs using the data from the grouped elements

	SYNTAX:
		{ $group: { _id: "$value" , fieldResult: "$valueResult" }	}

*/


db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", total: { $sum : "$stock"}}}
	]);


// SECTION - AGGREGATE PIPELINE
/*

using both $match and $group along with the aggregation will find for the products that are onsale and will group the total amount of stocks for all suppliers found

	- pipelines/aggregation process is the series of aggregation methods should the dev/client wants to use two or more aggregation methods in one statement. 
	- MongoDB will treat these as stages wherein it will not proceed to the next pipeline, unless it is done with the first.

	SYNTAX
		db.collectionName.aggregate([
			{ $match: {field: value}},
			{ $group: { _id: "$value" , fieldResult: "$valueResult" } }

		]);

*/

db.fruits.aggregate([
        { $match: {onSale : true} },
		{ $group: { _id: "$supplier_id", total: { $sum : "$stock"}}}
	]);



// SECTION - Field Projections with Aggregations
/*
	$project 
		- can be used when aggregating data to include/exclude fields from the returned result

	SYNTAX:
		{ $project: { field : value} },
*/
db.fruits.aggregate([
        { $match: {onSale : true} },
		{ $group: { _id: "$supplier_id", total: { $sum : "$stock"}}},
		{ $project: { _id: 0}}
	]);


/*
	$sort
		- used to change the order of aggregated results
			1 - ascending
			-1 - descending
	
	SYNTAX:
		{ $project: { field : 1/-1} },
*/

db.fruits.aggregate([
        { $match: {onSale : true} },
		{ $group: { _id: "$supplier_id", total: { $sum : "$stock"}}},
		{ $sort: { total: -1}}
	]);

/*
db.fruits.aggregate([
	  	{ $group: { _id: "$origin", kinds: {$sum: 1}}}
	]);
*/



/*
	code below wont work if the goal is to group the documents based on the number of times a country is mentioned in the "origin" array field.
*/
/*
db.fruits.aggregate([
	  	{ $group: { _id: "$origin", kinds: {$sum: 1}}}
	]);
*/





/*
	$unwind
		used to deconstruct an array field from a collection/field with array value to output a result for each element.

		this will result to creating seperate documents for each of the elements inside the array.

	SYNTAX:
		{ $unwind: "origin"};	

*/
db.fruits.aggregate([
	{$unwind: "$origin"},
	]);


// Using the code below, we are instructing MongoDB to deconstruct first the documents based on the origin field and group them based on tnhe number of times a country is mentioned in the "origin" array.
db.fruits.aggregate([
	{$unwind: "$origin"},
	{ $group: { _id: "$origin", kinds: {$sum: 1}}},
	]);



// SECTION - Schema design
/*
	- schema design/data modelling is an important feature when creating databases
	- MongoDB documents can be categorized into normalized or de-normalized/embedded data
	- Normalized data refers to a data structure where documents are referred to each other using their ids for related pieces of information

	- De-normalized data/embedded data design refers to a data structure where related pieces of information are added to a document as an embedded object.

*/


var owner = ObjectId();

db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact: "09123456789"
});


/*
miniactivity
	create "suppliers" collection
		- name - string
		- contacat - string
		- owner_id - similar id as our "John Smith" id

*/

db.suppliers.insert({
	name: "",
	contact: "",
	owner_id: ObjectId("62d5478f056a1e40a470f362")
});





// de-normalized data schema design with one-to-few relationship


db.suppliers.insert({
	name: "DEF Fruits",
	contact: "09123456789",
	address: [
		{street: "123 San Jose St.", city: "Manila"},
		{street: "367 Gil Puyat", city: "Makati"}
	]
});



/*
		-Both data structures are common practices, but each of them has its own pros and cons

	Difference between Normalized and De-Normalized


	NORMALIZED
		- easier to read information because seperate documents can be retrieved.

		- in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.


		- This approach is useful for data structures where pieces of information are commonly operated on/changed.
		
		
	DE-NORMALIZED
		- it makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve the documents.

		- if the data structure becomes too complex and long, it makes it more difficult to manipulate and access information

		- this approach is applicable for data structures where pieces of information are commonly retrieved and are rarely operated on/changed.
		
*/


/*
	MINIACTIVITY
		suppliers collection:
			create 2 variables with ObjectId() values
				supplier
				branch

			insert a document with the following properties
				_id: supplier,
				name: string,
				contact: string,
				branches: [
					branch
				]

			create a branch collection and insert an object with the following properties
				_id: similar as the "branch" field in suppliers collection
				name: string,
				address: string,
				city: string,
				supplier_id: the same id as the suppliers collection

*/


var supplier = ObjectId();
var branch = ObjectId();

db.suppliers.insert({
	_id: supplier,
	name: "XYZ Fruits",
	contact: "09123456789",
	branches: [
	branch
	]
});


db.branches.insert({
	_id: branch,
	name: "XYZ Cal",
	address: "123 Brgy. 254",
	city: "Caloocan",
	supplier_id: supplier,
	

});